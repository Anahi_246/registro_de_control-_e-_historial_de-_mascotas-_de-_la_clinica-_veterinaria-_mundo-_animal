<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence(rand(5,10));
        return [    
            'title'=> $title,
            'slug'=> str::slug($title),  
            'duenio'=> 'duenio x',
            'color'=> 'color x',
            'sexo'=> 'sexo x',
            'edad'=> '21',
            'direccion'=> 'direccion x',
            'fecha'=> 'fecha x',      
            'body' => $this->faker->paragraph(rand(200,500)),
            'user_id' => $this->faker->randomDigit(1,10),
            'category_id' => $this->faker->randomDigit(1,10),
         ];
    }
}
