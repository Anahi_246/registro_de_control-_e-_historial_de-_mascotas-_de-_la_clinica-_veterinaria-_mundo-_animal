<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string ('title');
            $table->string ('slug');
            $table->string ('duenio');
            $table->string ('color');
            $table->string ('sexo');
            $table->string ('edad');
            $table->string ('direccion');
            $table->string ('fecha');
            $table->text('body');
            $table->integer ('category_id');
            $table->integer ('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
